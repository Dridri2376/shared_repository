var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
// makes and return a required DOM element
export const selectDOMelt = (classname) => document.querySelector(classname);
// makes and return a click DOM event in order to affect HTML classes
export const clickAction = (parentValue, eventType, elt, value, option) => parentValue.addEventListener(eventType, () => { return elt.classList.toggle(value, option); });
export const pathTofile = "./../data/cv.json";
// get Request
export const get = (url) => __awaiter(void 0, void 0, void 0, function* () {
    const response = yield fetch(url);
    return response.json();
});
//# sourceMappingURL=helpers.js.map