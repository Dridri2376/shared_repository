import { selectDOMelt, clickAction, get, pathTofile } from "./logicHelpers/helpers.js";
const ModalIsActive = "is-active";



/**
 * SCHOOL SECTION
 */
let schoolCard = selectDOMelt( ".--SC-card" );
let schoolModal = selectDOMelt( ".--SC-modal" );
let schoolCloseBtn = selectDOMelt( '.button__close-SC-modal' );

// open Modal
clickAction( schoolCard, "click", schoolModal, ModalIsActive );
// close modal
clickAction( schoolCloseBtn, "click", schoolModal, ModalIsActive, true );

/**
 * XP SECTION
 */
let XPCard = selectDOMelt( ".--XP-card" );
let XPModal = selectDOMelt( ".--XP-modal" );
let XPCloseBtn = selectDOMelt( '.button__close-XP-modal' );

clickAction( XPCard, "click", XPModal, ModalIsActive );
clickAction( XPCloseBtn, "click", XPModal, ModalIsActive, true );

/**
 * OT SECTION
 */
let OTCard = selectDOMelt( ".--OT-card" );
let OTModal = selectDOMelt( ".--OT-modal" );
let OTCloseBtn = selectDOMelt( '.button__close-OT-modal' );

clickAction( OTCard, "click", OTModal, ModalIsActive );
clickAction( OTCloseBtn, "click", OTModal, ModalIsActive, true );


// personal section fullfillment
const fullName = selectDOMelt( ".fullName" );
get( pathTofile ).then( ( r ) => fullName.textContent = `${ r.personal_infos.firstName } ${ r.personal_infos.lastName }` );






const getCurrentAge = ( birthDate: Date ): string =>
{
    const diffInMillis = Date.now() - birthDate.getTime();
    const age_dt = new Date( diffInMillis );
    return Math.abs( age_dt.getUTCFullYear() - 1970 ).toString();
};
let currentAgeDomElt = selectDOMelt( '.currentAge' );
currentAgeDomElt.textContent = `${ getCurrentAge( new Date( 1987, 12, 27 ) ) } ans`;



