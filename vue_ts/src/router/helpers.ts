export const routerToPage = (router: any, route: string): any => {
    return router.push(route);
}
