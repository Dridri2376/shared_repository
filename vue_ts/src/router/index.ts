import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import HomeView from "../views/HomeView.vue";
import RegisterView from "../views/RegisterView.vue";

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    component: HomeView
  },
  {
    path: '/new',
    component: RegisterView
  }
]

const router = new VueRouter({
  routes
})

export default router
