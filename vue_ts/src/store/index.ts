import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    users: [],
  },
  getters: {
    allUsers: (state) => { return state.users },
    usersLength: (state) => { return state.users.length }
  },
  actions: {
    async getAllUsers({ commit }) {
      const response = await axios.get("https://jsonplaceholder.typicode.com/users");
      commit("GETALL", response.data);
    },

    async deleteOne({ commit }, id) {
      const deletedUser = await axios.delete(`https://jsonplaceholder.typicode.com/users/${id}`);
      commit("DELETE", id);
    },

    async addOne({ commit }, userdata) {
      const newUser = await axios.post("https://jsonplaceholder.typicode.com/users", { body: userdata })
      commit("NEWONE", newUser.data);
    },
  },
  mutations: {
    GETALL: (state, users) => {
      return state.users = users;
    },
    DELETE: (state, id) => {
      state.users = state.users.filter((user: any) => user.id !== id)
    },
    NEWONE: (state, new_user) => {
      return state.users.unshift(new_user.body);
    }
  },
  modules: {}

})
