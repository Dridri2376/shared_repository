export const sumQty = (array) => {
    let total = 0;
    for (const item of array)
    {
        total += item.quantity;
    }
    return total;
};

export const checkIfDuplicateExists = (array: []) => {
    return new Set(array).size !== array.length;
};

