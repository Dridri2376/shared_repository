// esentials
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
Vue.config.productionTip = false;

// bootstrap-vue design library
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

// validation library
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate);




// global instances
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
