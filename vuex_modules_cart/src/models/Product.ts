export default class Product {
    private id!: number;
    private title!: string;
    private price!: number;
    private category!: string;
    private description!: string;
    private quantity!: number;

}

