export const navigate = (router: any, route: string) => {
    return router.push(route);
};
