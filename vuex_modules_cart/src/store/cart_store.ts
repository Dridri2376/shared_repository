import Product from "@/models/Product";
import { productsStore } from "./products_store";

export const cartStore = {
    state: {
        cart: Array<Product>(),
        incrementCartValue: 1,
        incrementQtyValue: 0,
        incrementPriceValue: 0,
    },
    getters: {
        getCartList: (state) => state.cart,
        cartLength: (state) => state.cart.length + state.incrementCartValue,
        TotalCartPrice: (state) => state.incrementPriceValue,
    },
    actions: {
        increaseProductQty: async ({ commit }, product): Promise<any> => {
            try
            {
                commit("increaseProductQty", product);
            } catch (error)
            {
                throw error;
            }
        },
        decreaseProductQty: async ({ commit }, product): Promise<any> => {
            try
            {
                commit("decreaseProductQty", product);
            } catch (error)
            {
                throw error;
            }
        },
        removeProductFromCart: async ({ commit }, product): Promise<any> => {
            try
            {
                commit("removeProductFromCart", product);
            } catch (error)
            {
                throw error;
            }
        },
        increaseTotalCartQuantity: async ({ commit }, product): Promise<any> => {
            try
            {
                commit("increaseTotalCartQuantity", product);
            } catch (error)
            {
                throw error;
            }
        },
        decreaseTotalCartQuantity: async ({ commit }, product): Promise<any> => {
            try
            {
                commit("decreaseTotalCartQuantity", product);
                if (product.quantity < 1)
                {
                    commit("removeProductFromCart", product);
                }
            } catch (error)
            {
                throw error;
            }
        },
        increaseTotalCartPrice: async ({ commit }, price: number): Promise<any> => {
            try
            {
                commit("increaseTotalCartPrice", price);
            } catch (error)
            {
                throw error;
            }
        },
        decreaseTotalCartPrice: async ({ commit }, price: number): Promise<any> => {
            try
            {
                commit("decreaseTotalCartPrice", price);
            } catch (error)
            {
                throw error;
            }
        },
    },
    mutations: {
        increaseProductQty: (state, product) => {
            // make the product counter in cart increase FOR A SPECIFC PRODUCT
            const updatedProductQuantity: number = product.quantity += 1;
            return updatedProductQuantity;
        },

        decreaseProductQty: (state, product) => {
            // make the product counter in cart decrease FOR A SPECIFC PRODUCT
            const updatedProductQuantity: number = product.quantity -= 1;
            return updatedProductQuantity;
        },

        removeProductFromCart: (state, product) => {
            // delete product from cart VISUALLY
            const deletedproductValue = state.cart.find((po) => po == product);
            const deletedproductIndex = state.cart.findIndex((po) => po.id == product.id);
            state.cart.splice(deletedproductIndex, 1);
            productsStore.state.alreadyKnowProducts.splice(deletedproductIndex, 1);
            state.incrementPriceValue = state.incrementPriceValue -= deletedproductValue.price * deletedproductValue.quantity;
            product.quantity = 1;
            return state.cart;
        },

        increaseTotalCartQuantity: (state) => {
            // increment totalCartQty value
            state.incrementQtyValue++;
        },

        decreaseTotalCartQuantity: (state) => {
            // decrement totalCartQty value
            if (state.incrementQtyValue < 1)
            {
                state.incrementQtyValue = 0;
            } else
            {
                state.incrementQtyValue--;
            }
        },
        increaseTotalCartPrice: (state, price) => {
            // increase totalAmount of cart in €
            state.incrementPriceValue = state.incrementPriceValue += price;
            return state.incrementPriceValue;
        },
        decreaseTotalCartPrice: (state, price) => {
            // decrease totalAmount of cart in €
            state.incrementPriceValue = state.incrementPriceValue -= price;
            if (state.cart.length === 1) { state.incrementPriceValue = state.incrementPriceValue; }
            return state.incrementPriceValue;
        },
    },
};
