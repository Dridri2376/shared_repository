import Vue from 'vue';
import Vuex from 'vuex';
import { productsStore } from "./products_store";
import { cartStore } from "./cart_store";

Vue.use(Vuex);

export default new Vuex.Store({

  modules: {
    productsStore,
    cartStore,
  },
});
